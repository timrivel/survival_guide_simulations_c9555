# A Survival Guide to Simulations

Wanna start? Copy these files first to your local machine.

<code>git clone https://gitlab.com/denysbiriukov/survival_guide_simulations_c9555.git</code>

### Instruction for MUNI students

Before any practical lesson, be sure that the working environment is ready and functional.

Enter the folder of the corresponding practical, for instance:

<code>cd week1/</code>

Adjust the infinity environment for the needs of the course:

<code>bash ../infinity/infinity-modules.sh</code>

Initiate the conda and jupyter environment with all necessary packages:

<code>bash ../conda/conda-activate.sh</code>

Once the jupyter environment is enabled, open the jupyter notebook in your browser via the link popping up on the screen, and you ready to go!
